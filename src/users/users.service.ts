import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';
import { UsersEntity as UsersEntity } from './users.entity';

export const opciones: FindOneOptions<UsersEntity> = {
    select: ['id', 'name', 'password', 'active']
};

@Injectable()
export class UsersService {

    constructor(@InjectRepository(UsersEntity) private _usersRepository: Repository<UsersEntity>) { }

    async search() {
        return await this._usersRepository.find(opciones);
    }

    async create(usuario: UsersEntity) {
        const userCreate = await this._usersRepository.save(usuario);
        return this._usersRepository.findOne(userCreate.id, opciones);
    }

    async update(id: string, usuario: Partial<UsersEntity>) {
        await this._usersRepository.update(id, usuario);
        return await this._usersRepository.findOne(id, opciones);
    }

    async delete(id: string | string[]) {
        return await this._usersRepository.delete(id);
    }

}
