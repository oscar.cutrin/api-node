import { Column, CreateDateColumn, Entity, PrimaryColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { UserModel } from './users.model';

@Entity()
export class UsersEntity implements UserModel {

    @PrimaryGeneratedColumn()
    id: number;

    @PrimaryColumn()
    @Column({ nullable: false, length: 50, unique: true })
    name: string;

    @PrimaryColumn()
    @Column({ nullable: false })
    password: string;

    @Column({ nullable: false })
    active: boolean;

    @CreateDateColumn()
    createDate;

    @UpdateDateColumn()
    updateDate;

}