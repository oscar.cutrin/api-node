export interface UserModel {
    id: number;
    name: string;
    password: string;
    active: boolean;
}
