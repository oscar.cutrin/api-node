import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { bdConfig } from 'bd.config';
import { UsersController } from './users.controller';
import { UsersEntity } from './users.entity';
import { UsersService } from './users.service';

@Module({
    imports: [
        TypeOrmModule.forRoot(bdConfig('users')),
        TypeOrmModule.forFeature([UsersEntity])
    ],
    providers: [
        UsersService
    ],
    controllers: [
        UsersController
    ]
})
export class UsersModule { }
