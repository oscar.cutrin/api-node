import { Body, Controller, Delete, Get, HttpCode, Param, Patch, Post } from '@nestjs/common';
import { UsersEntity } from './users.entity';
import { UsersService } from './users.service';

@Controller('api/users')
export class UsersController {

    constructor(private _usersService: UsersService) { }

    @Get()
    async search() {
        return await this._usersService.search();
    }

    @Post()
    async create(@Body() user: UsersEntity) {
        return await this._usersService.create(user);
    }

    @Patch(':id')
    async update(@Body() user: UsersEntity, @Param('id') id: string) {
        return await this._usersService.update(id, user);
    }

    @HttpCode(204)
    @Delete(':id')
    async delete(@Param('id') id: string) {
        return await this._usersService.delete(JSON.parse(id));
    }

}
