import { NestFactory } from '@nestjs/core';
import { environtment } from 'environtment';
import { AppModule } from './app.module';

declare const module: any;

function setHeaders(app: any) {
    app.use((_req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header('Access-Control-Allow-Headers', 'Content-Type');
        res.header('Content-Type', 'application/json');
        next();
    });
}

function certificate(): { keyFile: any, certFile: any } {
    const fs = require('fs');
    return {
        keyFile: fs.readFileSync(__dirname + '/ssl/fwk-ng.key'),
        certFile: fs.readFileSync(__dirname + '/ssl/fwk-ng.crt')
    };
}

function httpsOptions(): { httpsOptions: any } {
    const cert = certificate();
    return {
        httpsOptions: {
            key: cert.keyFile,
            cert: cert.certFile,
        }
    };
}

async function bootstrap() {

    // const app = await NestFactory.create(AppModule, httpsOptions());
    const app = await NestFactory.create(AppModule);

    setHeaders(app);

    app.enableCors();

    await app.listen(environtment.server.port, environtment.server.host);

    if (module.hot) {
        module.hot.accept();
        module.hot.dispose(() => app.close());
    }
}

bootstrap();
