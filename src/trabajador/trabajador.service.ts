import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';
import { TrabajadorEntity as TrabajadorEntity } from './trabajador.entity';

export const opciones: FindOneOptions<TrabajadorEntity> = {
    select: ['id', 'codigo', 'nombre', 'apellidos']
};

@Injectable()
export class TrabajadorService {

    constructor(@InjectRepository(TrabajadorEntity) private _trabajadorRepository: Repository<TrabajadorEntity>) { }

    async search(entidad?: any) {
        if (entidad) {
            return await this._trabajadorRepository.find(entidad);
        } else {
            return await this._trabajadorRepository.find(opciones);
        }
    }

    async create(usuario: TrabajadorEntity) {
        const trabajador = await this._trabajadorRepository.save(usuario);
        return this._trabajadorRepository.findOne(trabajador.id, opciones);
    }

    async update(id: string, usuario: Partial<TrabajadorEntity>) {
        await this._trabajadorRepository.update(id, usuario);
        return await this._trabajadorRepository.findOne(id, opciones);
    }

    async delete(id: string | string[]) {
        return await this._trabajadorRepository.delete(id);
    }

}
