export interface TrabajadorModel {
    id: number | string;
    codigo: string;
    nombre: string;
    apellidos: string;
}
