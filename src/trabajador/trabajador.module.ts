import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { bdConfig } from 'bd.config';
import { TrabajadorController } from './trabajador.controller';
import { TrabajadorEntity } from './trabajador.entity';
import { TrabajadorService } from './trabajador.service';

@Module({
    imports: [
        TypeOrmModule.forRoot(bdConfig('ejemplos')),
        TypeOrmModule.forFeature([TrabajadorEntity])
    ],
    providers: [
        TrabajadorService
    ],
    controllers: [
        TrabajadorController
    ]
})
export class TrabajadorModule { }
