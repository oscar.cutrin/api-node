import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';
import { TrabajadorModel } from './trabajador.model';

@Entity()
export class TrabajadorEntity implements TrabajadorModel {

    @PrimaryGeneratedColumn()
    id: number | string = '';

    @PrimaryColumn()
    @Column({ nullable: false, length: 50, unique: true })
    codigo: string = '';

    @PrimaryColumn()
    @Column({ nullable: false })
    nombre: string = '';

    @Column({ nullable: false })
    apellidos: string = '';

}