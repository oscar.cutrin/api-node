import { Body, Controller, Delete, Get, HttpCode, Param, Patch, Post } from '@nestjs/common';
import { TrabajadorEntity } from './trabajador.entity';
import { TrabajadorService } from './trabajador.service';

@Controller('api/mantenimientos/listaficha/compleja')
export class TrabajadorController {

    constructor(private _trabajadorService: TrabajadorService) { }

    @Get()
    async buscar() {
        const e = await this._trabajadorService.search();
        return e;
    }
    @Get(':param')
    async buscarPorId(@Param('param') param?: string) {

        let e;

        if (typeof JSON.parse(param) === 'object') {
            let aa = {};
            Object.keys(JSON.parse(param)).forEach((res: any) => {
                if (JSON.parse(param)[res]) {
                    aa = { ...aa, ...{ [`${res}`]: JSON.parse(param)[res] as number } };
                }
            });
            e = await this._trabajadorService.search(aa);

        } else if (typeof param === 'string') {
            console.log('id - string', param);
            e = await this._trabajadorService.search(param);
        } else if (typeof param === 'number') {
            console.log('id - number', param);
            e = await this._trabajadorService.search(param);
        }

        return e;
    }

    @Post()
    async crear(@Body() trabajador: TrabajadorEntity) {
        const e = await this._trabajadorService.create(trabajador);
        return e.id;
    }

    @Patch(':id')
    async actualizar(@Body() trabajador: TrabajadorEntity, @Param('id') id: string) {
        const e = await this._trabajadorService.update(id, trabajador);
        return e;
    }

    @HttpCode(204)
    @Delete(':id')
    async eliminar(@Param('id') id: string) {
        let e: any;
        if (JSON.parse(id) instanceof Array) {
            for (const i of JSON.parse(id)) {
                e = await this._trabajadorService.delete(i);
            }
        } else {
            e = await this._trabajadorService.delete(id);
        }
        return e;

    }

}
