import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { environtment } from 'environtment';

export const bdConfig = (db: string): TypeOrmModuleOptions => {
    return {
        type: 'postgres',
        host: environtment.bd.postgres.host,
        port: environtment.bd.postgres.port,
        database: db,
        username: environtment.bd.postgres.username,
        password: environtment.bd.postgres.password,
        entities: [__dirname + '/../**/trabajador.entity{.ts,.js}'],
        synchronize: true
    };
};