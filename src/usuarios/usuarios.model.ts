export interface UsuariosModel {
    id: number;
    nombre: string;
    password: string;
    activo: boolean;
}
