import { Column, CreateDateColumn, Entity, PrimaryColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { UsuariosModel } from './usuarios.model';

@Entity()
export class UsuarioEntity implements UsuariosModel {

    @PrimaryGeneratedColumn()
    id: number;

    @PrimaryColumn()
    @Column({ nullable: false, length: 50, unique: true })
    nombre: string;

    @PrimaryColumn()
    @Column({ nullable: false })
    password: string;

    @Column({ nullable: false })
    activo: boolean;

    @CreateDateColumn()
    createDate;

    @UpdateDateColumn()
    updateDate;

}