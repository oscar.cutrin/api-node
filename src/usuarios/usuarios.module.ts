import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { bdConfig } from 'bd.config';
import { UsersController } from './usuarios.controller';
import { UsuarioEntity } from './usuario.entity';
import { UsuariosService } from './usuarios.service';

@Module({
    imports: [
        TypeOrmModule.forRoot(bdConfig('usuarios')),
        TypeOrmModule.forFeature([UsuarioEntity])
    ],
    providers: [
        UsuariosService
    ],
    controllers: [
        UsersController
    ]
})
export class UsuariosModule { }
