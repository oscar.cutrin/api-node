import { Body, Controller, Delete, Get, HttpCode, Param, Patch, Post } from '@nestjs/common';
import { UsuarioEntity } from './usuario.entity';
import { UsuariosService } from './usuarios.service';

@Controller('api/usuarios')
export class UsersController {

    constructor(private _usuariosService: UsuariosService) { }

    @Get()
    async buscar() {
        return await this._usuariosService.serach();
    }

    @Post()
    async crear(@Body() user: UsuarioEntity) {
        return await this._usuariosService.create(user);
    }

    @Patch(':id')
    async actualizar(@Body() user: UsuarioEntity, @Param('id') id: string) {
        return await this._usuariosService.update(id, user);
    }

    @HttpCode(204)
    @Delete(':id')
    async eliminar(@Param('id') id: string) {
        return await this._usuariosService.delete(JSON.parse(id));
    }

}
