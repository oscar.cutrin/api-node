import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';
import { UsuarioEntity as UsuarioEntity } from './usuario.entity';

export const opciones: FindOneOptions<UsuarioEntity> = {
    select: ['id', 'nombre', 'password', 'activo']
};

@Injectable()
export class UsuariosService {

    constructor(@InjectRepository(UsuarioEntity) private _usuariosRepository: Repository<UsuarioEntity>) { }

    async serach() {
        return await this._usuariosRepository.find(opciones);
    }

    async create(usuario: UsuarioEntity) {
        const userCreate = await this._usuariosRepository.save(usuario);
        return this._usuariosRepository.findOne(userCreate.id, opciones);
    }

    async update(id: string, usuario: Partial<UsuarioEntity>) {
        await this._usuariosRepository.update(id, usuario);
        return await this._usuariosRepository.findOne(id, opciones);
    }

    async delete(id: string | string[]) {
        return await this._usuariosRepository.delete(id);
    }

}
