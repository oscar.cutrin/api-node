import { Module } from '@nestjs/common';
import { TrabajadorModule } from 'trabajador/trabajador.module';
import { AppController } from './app.controller';

@Module({
    imports: [TrabajadorModule],
    controllers: [AppController],
})
export class AppModule { }
